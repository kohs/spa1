<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require __DIR__.'/../inc_login.php';

require_once __DIR__."/DB-DUMMY.php";
require_once __DIR__."/data/Sicht.php";
require_once __DIR__."/data/SystemRecht.php";

if((isset($_SESSION["id"]) && SystemRecht::hasStaticRolle("admin", $_SESSION["id"]))
        && isset($_GET["mode"]) && isset($_GET["id"])) {

    $out = null;
    $mode = $_GET["mode"];
    $id = $_GET["id"];

    $sicht = new Sicht($id);

    switch ($mode) {
        case "getAlleSichtgruppen" :
            $out = Sicht::getAlleSichtgruppen();
            break;

        case "getData" :
            $out = $sicht->getData();
            break;

        case "getSichtMitZugriff" :
            $out = $sicht->getSichtMitZugriff();
            break;

        case "getStruktSicht" :
            $out = $sicht->getStruktSicht();
            break;

        case "getAlleRollen" :
            $out = $sicht->getAlleRollen();
            break;

        case "getAlleSichten" :
            $out = $sicht->getAlleSichten();
            break;

        case "getAlleRechte" :
            $out = $sicht->getAlleRechte();
            break;

        case "getAlleModi" :
            $out = $sicht->getAlleModi();
            break;

        default :
            $out = "FEHLER!";
    }

    if (isset($_GET["edit"])) {

        $json = json_decode($_GET["edit"]);

        switch ($mode) {

            case "saveViewMode" :
                $out = $sicht->saveViewMode($json);
                break;

            default :
        }
    }

    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($out);
}