<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require_once __DIR__."/DB-DUMMY.php";
require_once __DIR__."/data/SystemRecht.php";

if(isset($_GET["grp"]) && isset($_GET["token"]) && isset($_GET["mode"])) {
    $out = null;

    $grp = $_GET["grp"];
    $view = isset($_GET["view"]) ? $_GET["view"] : "";
    $mode = $_GET["mode"];

    $token = json_decode($_GET["token"]);
    $sysRecht = new SystemRecht($grp, $token);

    switch ($mode) {

        case "getApiRecht" :
            $out = $sysRecht->getApiRecht($view);
            break;

        case "getUserId" :
            $out = $sysRecht->getUserId();
            break;

        case "getRechteOfGrp" :
            $out = $sysRecht->getRechteOfGrp("ap");
            break;

        case "isAdmin" :
            $out = $sysRecht->hasRolle("admin");
            break;

        default :
            $out = "FEHLER!";
    }




    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($out);
}