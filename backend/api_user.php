<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require_once __DIR__."/DB-DUMMY.php";
require_once __DIR__ . "/data/User.php";
require_once __DIR__."/data/SystemRecht.php";


if(isset($_GET["mode"]) && isset($_GET["id"]) && isset($_GET["token"])) {
    $out = null;
    $mode = $_GET["mode"];
    $id = $_GET["id"];

    $status = 1;
    if(isset($_GET["status"])) {
        $status = $_GET["status"];
    }

    $token = json_decode($_GET["token"]);

    $systemRecht = new SystemRecht("api_user", $token);
    $recht = $systemRecht->getApiRecht($mode);

    if($recht != "n") {

        $user = new User($id);

        switch ($mode) {
            case "getData" :
            case "getUser" :
                $out = $user->getData();
                break;

            default :
                $out = "FEHLER!";
        }

        if (isset($_GET["edit"])) {

            $json = json_decode($_GET["edit"]);

            switch ($mode) {
                case "addUser":
                case "addData":
                    $out = User::addData($json);
                    break;

                case "editData":
                    $out = $user->editData($json);
                    break;

                default :
                    $out = "FEHLER";
            }
        }
    } else {
        $out = -10;
    }

    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($out);
}