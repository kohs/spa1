<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

function getPHPDBOBJ($JSONSTR) {
    $cfg = json_decode($JSONSTR);
    $DB = new PDO($cfg->dns, $cfg->user, $cfg->password);
    $DB->dbtrys = $cfg->dbtrys;
}

function decrypt() {
    //https://stackoverflow.com/questions/3422759/php-aes-encrypt-decrypt
}

function encrypt() {
    //https://stackoverflow.com/questions/3422759/php-aes-encrypt-decrypt
}