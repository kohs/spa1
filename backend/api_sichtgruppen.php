<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
require_once __DIR__."/DB-DUMMY.php";
require_once __DIR__."/data/SichtGruppe.php";
require_once __DIR__."/data/SystemRecht.php";


if(isset($_GET["mode"]) && isset($_GET["id"])) {
    $out = null;
    $mode = $_GET["mode"];
    $id = $_GET["id"];

    $sichtGrp = new SichtGruppe($id);

    switch ($mode) {
        case "getAlleSichtgruppen" :
            $out = SichtGruppe::getAlleSichtgruppen();
            break;

        case "getSichten" :
            $out = $sichtGrp->getSichten();
            break;

        case "getData" :
            $out = $sichtGrp->getData();
            break;

        default :
            $out = "FEHLER!";
    }

    if (isset($_GET["edit"])) {

        $json = json_decode($_GET["edit"]);

        switch ($mode) {

            default :
        }
    }

    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($out);
}