<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

function getDbCfg($key) {
    $CFGDB = new stdClass();
    $CFGDB->dns = "mysql:dbname=spa;host=localhost";
    $CFGDB->user = "spa";     //TODO Datenbank User wechseln
    $CFGDB->password = "7ZYzh34HQ(asd)xzJIdVVrP65";
    $CFGDB->dbtrys = 3;
    return $CFGDB;
}

function getWebCfg($key) {
    $CFGWEB = new stdClass();
    $CFGWEB->wwwroot = 'http://'.$_SERVER['SERVER_NAME'];
    $CFGWEB->dirroot = __DIR__.'/../';
    return $CFGWEB;
}
