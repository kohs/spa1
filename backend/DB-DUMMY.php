<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require_once __DIR__.'/CFG.php';
require_once __DIR__.'/data/DbEditInsert.php';

GLOBAL $PDO;
GLOBAL $DdEditInsert;

function getPHPDBOBJ($JSONSTR) {
    GLOBAL $PDO;
    $cfg = getDbCfg("");

    if(empty($PDO)) {
        try {
            $PDO = new PDO($cfg->dns, $cfg->user, $cfg->password);
        } catch (PDOException $e) {
            echo '<h1>Verbindung fehlgeschlagen informieren Sie den Administrator.</h1>';
            exit();
        }
    }
    return $PDO;
}

function getDbEditInsert() {
    GLOBAL $DdEditInsert;

    if(empty($DdEditInsert)) {
        $DdEditInsert = new DbEditInsert();
    }
    return $DdEditInsert;
}

function decrypt() {
    //https://stackoverflow.com/questions/3422759/php-aes-encrypt-decrypt
}

function encrypt() {
    //https://stackoverflow.com/questions/3422759/php-aes-encrypt-decrypt
}
