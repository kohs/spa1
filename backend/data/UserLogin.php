<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require_once __DIR__.'/../DB-DUMMY.php';
require_once __DIR__.'/Session.php';

class UserLogin {

    /** @var  PDO  */
    private $PDO;
    private $sessionLib;
    private static $FIXSALT = "mk2015";     //nicht aendern
    //TODO loginTime umsetzten
    private static $MAXLOGINTIME = 50400;   //14Stunden;

    public function __construct() {
        $this->PDO = getPHPDBOBJ("");
        $this->sessionLib = new Session();
    }

    public static function logoutUser() {
        $extId = isset($_SESSION["id"]) ? $_SESSION["id"] : null;
        $keywert = isset($_SESSION["key"]) ? $_SESSION["key"] : null;

        if(!empty($extId) && !empty($keywert)) {

            $SQL = "DELETE FROM tmp_session WHERE ext_user = :ext AND keywert = :key";
            $PDO = getPHPDBOBJ("");
            $sth = $PDO->prepare($SQL);
            $sth->execute(array(":ext" => $extId, ":key" => $keywert));
        }
    }

    public static function getUserIdByToken($token) {
        if(isset($token->ext_user) & !empty($token->ext_user)) {

            $SQL = "SELECT id FROM tbl_user WHERE id_ext = :ext";

            $PDO = getPHPDBOBJ("");
            $sth = $PDO->prepare($SQL);
            $sth->execute(array(":ext" => $token->ext_user));

            $user = $sth->fetchObject();
            return isset($user->id) ? $user->id : 0;
        }
        return null;
    }

    public static function getToken() {
        if(isset($_SESSION["id"]) & !empty($_SESSION["id"])) {
            $SQL = "SELECT tmp_session.ext_user, tmp_session.apikey FROM tmp_session WHERE ext_user = :ext";

            $PDO = getPHPDBOBJ("");
            $sth = $PDO->prepare($SQL);
            $sth->execute(array(":ext" => $_SESSION["id"]));

            $user = $sth->fetchObject();
            return $user;
        }
        return null;
    }

    public function getPersonId() {
        if(session_status() == PHP_SESSION_DISABLED) {
            session_start();
        }
        if(isset($_SESSION["id"]) & !empty($_SESSION["id"])) {
            $SQL = "SELECT tbl_person.id FROM tbl_user, tbl_person WHERE tbl_user.fs_person = tbl_person.id AND id_ext = :ext";

            $sth = $this->PDO->prepare($SQL);
            $sth->execute(array(":ext" => $_SESSION["id"]));

            $user = $sth->fetchObject();
            return $user->id;
        }
        return null;
    }

    public function getUserId() {
        if(session_status() == PHP_SESSION_DISABLED) {
            session_start();
        }
        if(isset($_SESSION["id"]) & !empty($_SESSION["id"])) {
            $SQL = "SELECT id FROM tbl_user WHERE id_ext = :ext";

            $sth = $this->PDO->prepare($SQL);
            $sth->execute(array(":ext" => $_SESSION["id"]));

            $user = $sth->fetchObject();
            return $user->id;
        }
        return null;
    }

    public function isAdmin() {
        $userId = $this->getUserId();

        $SQL = "SELECT id FROM ztbl_user_rolle WHERE fs_user = :user AND fs_rolle = 5";

        $sth = $this->PDO->prepare($SQL);
        $sth->execute(array(":user" => $userId));

        $da = $sth->fetchObject();

        if(isset($da->id) && $da->id > 0) {
            return true;
        }
        return false;
    }

    public function login($username, $passwort) {
        $user = $this->getUserByLogin($username);
        if($user != false && is_object($user)) {
            if($user->passwort == crypt($passwort, $user->passwort)) {
                $session = $this->sessionLib->createSessionKey();
                if($this->sessionLib->bindSession($user->id_ext, $session)) {
                    return $user;
                } else {
                    return false;
                }
            }
        }

        return false;
    }

    public function isApiCorrect($id_ext, $apikey) {
        $loggedIn = false;
        $SQL = "SELECT * FROM tmp_session WHERE ext_user = :ext AND apikey = :apikey";

        $sth = $this->PDO->prepare($SQL);
        if(!empty($id_ext) && !empty($apikey)) {
            $exc_succses = $sth->execute(array(":ext" => $id_ext, ":apikey" => $apikey));
            if($exc_succses) {
                $exc_data = $sth->fetchObject();
                if(isset($exc_data->ext_user) && $exc_data->ext_user == $id_ext) {
                    $loggedIn = true;
                }
            }
        }
        return $loggedIn;
    }

    public function isLogedinIn($id_ext, $sessionkey) {
        $loggedIn = false;
        $SQL = "SELECT * FROM tmp_session WHERE ext_user = :ext AND keywert = :sessionkey";

        $sth = $this->PDO->prepare($SQL);
        if(!empty($id_ext) && !empty($sessionkey)) {
            $exc_succses = $sth->execute(array(":ext" => $id_ext, ":sessionkey" => $sessionkey));
            if($exc_succses) {
                $exc_data = $sth->fetchObject();
                if(isset($exc_data->ext_user) && $exc_data->ext_user == $id_ext) {
                    $loggedIn = true;
                }
            }
        }
        return $loggedIn;
    }

    public function refreshSession($id_ext, $oldSessionKey) {
        return $this->sessionLib->renewSession($id_ext, $oldSessionKey);
    }

    private function getUserByLogin($login) {
        $SQL = "SELECT * FROM tbl_user WHERE login = :login";

        $sth = $this->PDO->prepare($SQL);
        $sth->execute(array(":login" => $login));
        $user = $erg = $sth->fetchObject();

        if(empty($user) || strtolower($user->login) != strtolower($login)) {
            return null;
        }
        return $user;
    }
}