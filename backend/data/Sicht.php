<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require_once __DIR__.'/../DB-DUMMY.php';
require_once __DIR__ . '/BeanModus.php';
require_once __DIR__.'/AbsDbClass.php';
require_once __DIR__ . '/BeanRecht.php';
require_once __DIR__ . '/Rolle.php';

class Sicht extends AbsDbClass {

    private $PDO;
    private $id;

    public function __construct($Id) {
        $this->PDO = getPHPDBOBJ("");
        $this->id = $Id;
    }

    public function getData() {
        $SQL = "SELECT * FROM tbl_sicht WHERE id = :id";
        $array = array("id" => $this->id);

        return Sicht::prepareAndFetchMe($SQL, $array);
    }

    public function getStruktSicht () {
        $SQL = "SELECT si.*, zu.id AS fs_zugriff, zu.fs_sicht, zu.fs_rolle, zu.modus, zu.recht FROM tbl_sicht AS si LEFT JOIN ztbl_zugriff AS zu ON si.id = zu.fs_sicht WHERE si.id = :id ORDER BY si.id, zu.modus";
        $array = array("id" => $this->id);

        $dateArr =  Sicht::prepareAndFetchArray($SQL, $array);

        $outData = null;
        foreach($dateArr as $data) {

            if($outData == null) {
                $dataCl = new stdClass();
                $dataCl->id = $data->id;
                $dataCl->fs_sichtgruppe = $data->fs_sichtgruppe;
                $dataCl->name = $data->name;
                $dataCl->mode = array();
                $outData = $dataCl;
            }

            $rolle = new stdClass();
            $rolle->fs_rolle = $data->fs_rolle;
            $rolle->recht = $data->recht;

            if(!isset($outData->mode[$data->modus])) {
                $outData->mode[$data->modus] = array();
            }
            $outData->mode[$data->modus][$rolle->fs_rolle] = $rolle;
        }
        return $outData;
    }

    public function getSichtMitZugriff () {
        $SQL = "SELECT si.*, zu.id AS fs_zugriff, zu.fs_sicht, zu.fs_rolle, zu.modus, zu.recht FROM tbl_sicht AS si LEFT JOIN ztbl_zugriff AS zu ON si.id = zu.fs_sicht  WHERE si.id = :id";
        $array = array("id" => $this->id);

        return Sicht::prepareAndFetchArray($SQL, $array);
    }

    public function saveViewMode($data) {
        /*{"id":"1","fs_sichtgruppe":"1","name":"api_projekt->getData","mode":
         *      {"ap":
         *          {   "1":{"fs_rolle":1,"recht":"n"},
         *              "2":{"fs_rolle":2,"recht":"a"},
         *              "3":{"fs_rolle":3,"recht":"a"},
         *              "4":{"fs_rolle":4,"recht":"a"}}}}
         */

        $SQL_del = "DELETE FROM ztbl_zugriff WHERE fs_sicht = :fs_sicht
                    AND modus = :modus AND fs_rolle = :fs_rolle";

        $SQL_ins = "INSERT INTO ztbl_zugriff
                    SET fs_sicht = :fs_sicht,
                     modus = :modus,
                     fs_rolle = :fs_rolle,
                     recht = :recht";

        $PDO = getPHPDBOBJ("");
        $PDO->beginTransaction();

        $stm_del = $PDO->prepare($SQL_del);
        $stm_ins = $PDO->prepare($SQL_ins);

        $obj_del = array();
        $obj_ins = array();

        $obj_del["fs_sicht"] = $data->id;
        $obj_ins["fs_sicht"] = $data->id;

        $success = true;

        foreach($data->mode as $mkey => $mode) {
            $obj_del["modus"] = $mkey;
            $obj_ins["modus"] = $mkey;

            foreach($mode as $rol) {
                $obj_del["fs_rolle"] = $rol->fs_rolle;
                $obj_ins["fs_rolle"] = $rol->fs_rolle;

                if(isset($rol->recht) && !empty($rol->recht)) {
                    $obj_ins["recht"] = $rol->recht;

                    if($success) {
                        $success &= $stm_del->execute($obj_del);

                        if($success) {
                            $success &= $stm_ins->execute($obj_ins);
                        }
                    }
                }
            }
        }

        if($success) {
            $PDO->commit();
        } else {
            $PDO->rollBack();
        }

        return $success;
    }

    /* -- -- -- -- -- */

    public static function getAlleRollen () {
        return Rolle::getAlleRollen();
    }

    public static function getAlleSichten () {
        $SQL = "SELECT * FROM tbl_sicht";
        return Sicht::queryAndFetchArray($SQL);
    }

    public static function getAlleSichtgruppen () {
        $SQL = "SELECT * FROM ref_sichtgruppe";
        return Sicht::queryAndFetchArray($SQL);
    }

    public static function getAlleRechte () {
        return Recht::getAlleRechte();
    }

    public static function getAlleModi () {
        return Modus::getAlleModi();
    }

}