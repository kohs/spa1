<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

class DbEditInsert {

    private $PDO;

    public function __construct() {
        $this->PDO = getPHPDBOBJ("");
    }

    private function runSelectStmt($tbl, $select, $where, $orderby = null) {
        $SQL ="SELECT ";
        $whereArr = array();
        $error = false;

        //iteration über alle select
        $idIsSet = false;
        $first = true;
        foreach($select as $se) {
            if($first) {
                $SQL .= " $se ";
                $first = false;
            } else {
                $SQL .= ", $se ";
            }
            if($se == "id") {
                $idIsSet = true;
            }
        }

        //die ID muss immer mit selected werden
        if(!$idIsSet) {
            $SQL .= ",id";
        }

        $SQL .= " FROM $tbl ";

        if($first) {
            $error = true;
        }

        //iteration über alle where
        $first = true;
        foreach($where as $we => $val) {
            if($first) {
                $SQL .= " WHERE $we = :$we ";
                $first = false;
            } else {
                $SQL .= " AND $we = :$we ";
            }
            $whereArr[":$we"] = $val;
        }

        if($orderby != null) {
            $SQL .= " ORDER BY $orderby ASC";
        }

        if(!$error) {
            $sth = $this->PDO->prepare($SQL);
            $sth->execute($whereArr);
            return $sth;
        } else {
            return null;
        }
    }

    public function beginTransaction() {
        $ret = false;
        if(!$this->PDO->inTransaction()) {

            $ret = $this->PDO->beginTransaction();
        }
        return $ret;
    }

    public function commitTransaction() {
        $ret = false;
        if($this->PDO->inTransaction()) {

            $ret = $this->PDO->commit();
        }
        return $ret;
    }

    public function rollBackTransaction() {
        $ret = false;
        if($this->PDO->inTransaction()) {

            $ret = $this->PDO->rollBack();
        }
        return $ret;
    }

    public function checkAttributeSet($tbl, $toCheck) {
        $SQL = "SELECT id FROM $tbl ";

        $whereArr = array();

        //iteration über alle where
        $first = true;
        foreach($toCheck as $we => $val) {
            if($first) {
                $SQL .= " WHERE $we = :$we ";
                $first = false;
            } else {
                $SQL .= " AND $we = :$we ";
            }
            $whereArr[":$we"] = $val;
        }

        $sth = $this->PDO->prepare($SQL);
        $sth->execute($whereArr);

        $feObj = $sth->fetchObject();

        return isset($feObj->id) && !empty($feObj->id);
    }

    public function getData($tbl, $select, $where) {

        $sth = $this->runSelectStmt($tbl, $select, $where);

        //auslesen des Statements, wenn keine error aufgetreten ist
        if(!empty($sth)) {

            return $sth->fetchObject();
        } else {
            //FEHLERFALL
            return false;
        }
    }

    public function getDatas($tbl, $select, $where) {

        $sth = $this->runSelectStmt($tbl, $select, $where);

        //auslesen des Statements, wenn keine error aufgetreten ist
        if(!empty($sth)) {

            $outArr = array();
            while ($data = $sth->fetchObject()) {
                if(isset($data->id)) {
                    $outArr[$data->id] = $data;
                }
            }
            return $outArr;
        } else {
            //FEHLERFALL
            return false;
        }
    }

    public function getDataArr($tbl, $select, $where, $orderby = null) {

        $sth = $this->runSelectStmt($tbl, $select, $where, $orderby);

        //auslesen des Statements, wenn keine error aufgetreten ist
        if(!empty($sth)) {

            $outArr = array();
            while ($data = $sth->fetchObject()) {
                if(isset($data->id)) {
                    $outArr[] = $data;
                }
            }
            return $outArr;
        } else {
            //FEHLERFALL
            return false;
        }
    }

    public function createInsertSqlSet($tbl, $data) {
        $SQL = " INSERT INTO $tbl ";
        $first = true;

        $insertArr = array();
        foreach($data as $key => $d) {
            if($first) {
                $SQL .= " SET " . $key . " = :" . $key;
                $first = false;
            } else {
                $SQL .= ", " . $key . " = :" . $key;
            }
            $insertArr[":$key"] = $d;
        }

        $out = new stdClass();
        $out->sql = $SQL;
        $out->data = $insertArr;

        return $out;
    }

    /**
     * Bau ein INSERT-STMT auf und führt dieses aus.
     * @param $tbl String Tabellenname
     * @param $data array | stdClass assoziatives Array mit key gleich-Tabellen Attribut-Name
     * @return int 0 bei Error / Id > 0 bei Success
     */
    public function addInsert($tbl, $data) {
        $set = $this->createInsertSqlSet($tbl, $data);

        $sth = $this->PDO->prepare($set->sql);
        $ret = $sth->execute($set->data);
        if($ret) {
            $out = $this->PDO->lastInsertId();
            return $out;
        }
        return 0;
    }

    /**
     * Bau ein INSERT-STMT auf und führt dieses aus.
     * @param $tbl String Tabellenname
     * @param $data array | stdClass assoziatives Array mit key gleich-Tabellen Attribut-Name
     * @return bool false bei Error / true bei Success
     */
    public function addInsertFestId($tbl, $data) {
        $set = $this->createInsertSqlSet($tbl, $data);

        $sth = $this->PDO->prepare($set->sql);
        return $sth->execute($set->data);
    }

    public function addInsertNotAuto($tbl, $data) {
        $set = $this->createInsertSqlSet($tbl, $data);

        $sth = $this->PDO->prepare($set->sql);
        $ret = $sth->execute($set->data);
        if($ret) {
            return $data->id;
        }
        return 0;
    }

    /**
     * Bau ein UPDATE-STMT auf und führt dieses aus.
     * @param $tbl String Tabellename
     * @param $data array | stdClass assoziatives Array mit UPDATE-Daten und key gleich-Tabellen Attribut-Name
     * @param $where array | stdClass assoziatives Array mit WHERE-Daten und key gleich-Tabellen Attribut-Name
     * @return bool true==funtkioniert
     */
    public function addUpdate($tbl, $data, $where) {
        $SQL = " UPDATE $tbl ";
        $first = true;

        $insertArr = array();
        foreach($data as $key => $d) {
            if($first) {
                $SQL .= " SET " . $key . " = :" . $key;
                $first = false;
            } else {
                $SQL .= ", " . $key . " = :" . $key;
            }
            $insertArr[":$key"] = $d;
        }

        $first = true;
        foreach($where as $key => $d) {
            if($first) {
                $SQL .= " WHERE " . $key . " = :whher" . $key;
                $first = false;
            } else {
                $SQL .= " AND " . $key . " = :whher" . $key;
            }
            $insertArr[":whher$key"] = $d;
        }

        $sth = $this->PDO->prepare($SQL);

        return $sth->execute($insertArr);
    }
}