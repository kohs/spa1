<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require_once __DIR__.'/../DB-DUMMY.php';
require_once __DIR__ . '/BeanModus.php';
require_once __DIR__.'/AbsDbClass.php';
require_once __DIR__ . '/BeanRecht.php';

class SichtGruppe extends AbsDbClass {

    private $PDO;
    private $id;

    public function __construct($Id) {
        $this->PDO = getPHPDBOBJ("");
        $this->id = $Id;
    }

    public static function getAlleSichtgruppen () {
        $SQL = "SELECT * FROM ref_sichtgruppe";
        return SichtGruppe::queryAndFetchArray($SQL);
    }

    public function getData() {
        $SQL = "SELECT * FROM ref_sichtgruppe WHERE id = :id";
        $array = array("id", $this->id);

        return SichtGruppe::prepareAndFetchMe($SQL, $array);
    }

    private function appentSicht($rohdata, &$appendOn) {
        if(!isset($appendOn[$rohdata->id])) {
            $dataCl = new stdClass();
            $dataCl->id = $rohdata->id;
            $dataCl->fs_sichtgruppe = $rohdata->fs_sichtgruppe;
            $dataCl->name = $rohdata->name;
            $dataCl->mode = array();
            $appendOn[$rohdata->id] = $dataCl;
        }
    }

    public function getSichten() {
        $SQL = "SELECT si.*, zu.id AS fs_zugriff, zu.fs_sicht, zu.fs_rolle, zu.modus, zu.recht FROM tbl_sicht AS si LEFT JOIN ztbl_zugriff AS zu ON si.id = zu.fs_sicht WHERE si.fs_sichtgruppe = :id ORDER BY si.id, zu.modus";
        $array = array("id" => $this->id);

        $dateArr =  SichtGruppe::prepareAndFetchArray($SQL, $array);

        $outData = array();
        foreach($dateArr as $data) {
            $this->appentSicht($data, $outData);

            $rolle = new stdClass();
            $rolle->fs_rolle = $data->fs_rolle;
            $rolle->recht = $data->recht;
            if(!isset($outData[$data->id]->mode[$data->modus])) {
                $outData[$data->id]->mode[$data->modus] = array();
            }
            $outData[$data->id]->mode[$data->modus][$rolle->fs_rolle] = $rolle;
        }
        return $outData;
    }
}