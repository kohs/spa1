<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require_once __DIR__.'/UserLogin.php';

class SystemRecht
{

    /**
     * @var String Name der API oder Gruppe
     */
    private $api;
    private $userId;
    private $PDO;

    /**
     * @param $api_name string Name der API oder Sicht-Gruppe
     * @param $token
     */
    public function __construct($api_name, $token) {
        $this->PDO = getPHPDBOBJ("");
        $this->api = $api_name;
        $login = new UserLogin();
        if($login->isApiCorrect($token->ext_user, $token->apikey)) {
            $this->userId = UserLogin::getUserIdByToken($token);
        }
    }

    public function getUserId() {
        return $this->userId;
    }

    public function hasRolle($rollenname) {
        $rollen = $this->getRollen();

        foreach($rollen AS $r) {
            if(strtolower($r->name) == strtolower($rollenname)) {
                return true;
            }
        }

        return false;
    }

    public static function hasStaticRolle($rollenname, $extId) {

        $SQL = "SELECT r.*
                FROM tbl_user AS u
                INNER JOIN ztbl_user_rolle AS ur
                ON u.id = ur.fs_user
                INNER JOIN ref_rolle AS r
                ON ur.fs_rolle = r.id
                WHERE u.id_ext = :id";

        $PDO = getPHPDBOBJ("");
        $stmt = $PDO->prepare($SQL);
        $stmt->execute(array(":id" => $extId));

        while($obj = $stmt->fetchObject()) {
            if(isset($obj->id) && !empty($obj->id)) {
                if(strtolower($obj->name) == strtolower($rollenname)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getRollen($userId = null) {
        if($userId == null) {
            $userId = $this->userId;
        }

        $SQL = "SELECT r.*
                FROM ztbl_user_rolle AS ur
                INNER JOIN ref_rolle AS r
                ON ur.fs_rolle = r.id
                WHERE ur.fs_user = :id";

        $stmt = $this->PDO->prepare($SQL);
        $stmt->execute(array(":id" => $userId));

        $arr = array();
        while($obj = $stmt->fetchObject()) {
            if(isset($obj->id) && !empty($obj->id)) {
                $arr[] = $obj;
            }
        }

        return $arr;
    }

    public function getApiRecht($view) {
        return $this->getRecht($view, "ap");
    }

    public function getRechteOfGrp($modus) {

        $views = $this->getSichtenOfGrp($this->api);
        $arr = array();

        foreach($views as $v) {
            $arr[$v->name] = $this->getRecht($v->name, $modus, false);
        }

        return $arr;
    }

    private function getSichtenOfGrp($grp) {
        $SQL = "SELECT si.* FROM ref_sichtgruppe AS sg, tbl_sicht AS si
                WHERE sg.id = si.fs_sichtgruppe
                AND sg.kennung = :grp";

        $args = array(":grp" => $grp);

        $stmt = $this->PDO->prepare($SQL);
        $stmt->execute($args);

        $arr = array();
        while($obj = $stmt->fetchObject()) {
            if(isset($obj->id) && !empty($obj->id)) {
                $arr[] = $obj;
            }
        }

        return $arr;
    }

    public function getRecht($view, $modus, $prefix = true) {
        if($this->userId > 0) {

            $apiView = $prefix ? $this->api . "." . $view : $view;
            return SystemRecht::getStaticRecht($this->userId, $apiView, $modus);
        } else {
            return "n";
        }
    }

    public static function getStaticRecht($fs_user, $view, $modus) {
        $PDO = getPHPDBOBJ("");
        $SQL = "SELECT recht
            FROM tbl_sicht AS si, ztbl_zugriff AS zu, ref_rolle AS ro, ztbl_user_rolle AS ur
            WHERE zu.fs_sicht = si.id
              AND zu.fs_rolle = ro.id
              AND ro.id = ur.fs_rolle
              AND fs_user = :fs_user
              AND si.name = :viewname
              AND zu.modus = :modus";

        $args = array(":fs_user" => $fs_user, ":viewname" => $view, ":modus" => $modus);

        $stmt = $PDO->prepare($SQL);
        $stmt->execute($args);

        $rechteArr = array("a" => 0, "e" => 0, "n" => 0);

        while($obj = $stmt->fetchObject()) {
            $rechteArr[(isset($obj->recht) ? $obj->recht : "n")]++;
        };

        if($rechteArr["a"] > 0) {
            return "a";
        } elseif($rechteArr["e"] > 0) {
            return "e";
        } else {
            return "n";
        }
    }
}