<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require_once __DIR__.'/../DB-DUMMY.php';;
require_once __DIR__.'/AbsDbClass.php';

class Rolle extends AbsDbClass
{
    private $PDO;
    private $userId;

    public function __construct($Id) {
        $this->PDO = getPHPDBOBJ("");
        $this->userId = $Id;
    }

    private function hasRolle($rId) {
        $SQL = "SELECT * FROM ztbl_user_rolle WHERE fs_rolle = :rid AND fs_user = :uid";
        $erg = $this->prepareAndFetchMe($SQL, array(":rid" => $rId, ":uid" => $this->userId));
        return isset($erg->id) && !empty($erg->id) ? true : false;
    }

    public function getRollenTab () {
        $rollen = self::getAlleRollen();
        foreach ($rollen AS $r) {
            $r->hasRole = $this->hasRolle($r->id);
        }
        return $rollen;
    }

    public function setRollenTab($data) {

        $this->PDO->beginTransaction();

        $SQL_del ="DELETE FROM ztbl_user_rolle WHERE fs_user = :id";
        $sth = $this->PDO->prepare($SQL_del);
        $ok = $sth->execute(array(":id" => $this->userId));

        $SQL_add = "INSERT INTO ztbl_user_rolle SET fs_rolle = :fs_rolle, fs_user = :fs_user";
        $sth_add = $this->PDO->prepare($SQL_add);

        foreach($data AS $d) {
            if(!empty($d->hasRole) && $d->hasRole) {
                $ok |= $sth_add->execute(array(":fs_rolle" => $d->id, ":fs_user" => $this->userId));
            }
        }

        if($ok) {
            $this->PDO->commit();
            return 1;
        } else {
            $this->PDO->rollBack();
            return -1;
        }
    }

    /* **** */

    public static function getAlleRollen () {
        $SQL = "SELECT * FROM ref_rolle";
        return self::queryAndFetchArray($SQL);
    }
}