<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

$CFG = getWebCfg("");

require_once __DIR__.'/../DB-DUMMY.php';

class Session {

    /** @var  PDO */
    private $PDO;

    public function __construct() {
        $this->PDO = getPHPDBOBJ("");
    }

    public function bindSession($id_ext, $sessionkey)
    {
        $success = false;

        $akt_sess = $this->selectSession($id_ext);
        if(!empty($akt_sess) && !empty($akt_sess->ext_user) && $akt_sess->ext_user == $id_ext) {
            if($akt_sess->keywert == $sessionkey) {
                $success = true;
            } else {
                $this->updateSession($id_ext, $sessionkey);
                $_SESSION['id'] = $id_ext;
                $_SESSION['key'] = $sessionkey;
                $success = true;
            }
        } else {
            $success = $this->insertSession($id_ext, $sessionkey);
            $_SESSION['id'] = $id_ext;
            $_SESSION['key'] = $sessionkey;
        }

        return $success;
    }

    private function selectSession($id_ext) {
        $SQL = "SELECT * FROM tmp_session WHERE ext_user = :ext";

        $sth = $this->PDO->prepare($SQL);
        $success = $sth->execute(array(":ext" => $id_ext));

        return $sth->fetchObject();
    }

    private function updateSession($id_ext, $sessionkey)
    {
        $SQL = "UPDATE tmp_session SET keywert = :session, apikey = :apikey WHERE ext_user = :ext";
        $trys = 0;
        $success = false;

        $apikey = $this->createApiKey();

        while (!$success && $trys < 3) {
            $sth = $this->PDO->prepare($SQL);
            $success = $sth->execute(array(":session" => $sessionkey, ":ext" => $id_ext, ":apikey" => $apikey));
            $trys++;
        }
        return $success;
    }

    /*
     * Ohne ApiKey-Update
     */
    private function updateSessionOnly($id_ext, $sessionkey)
    {
        $SQL = "UPDATE tmp_session SET keywert = :session WHERE ext_user = :ext";
        $trys = 0;
        $success = false;

        $apikey = $this->createApiKey();

        while (!$success && $trys < 3) {
            $sth = $this->PDO->prepare($SQL);
            $success = $sth->execute(array(":session" => $sessionkey, ":ext" => $id_ext));
            $trys++;
        }
        return $success;
    }

    private function insertSession($id_ext, $sessionkey)
    {
        $SQL = "INSERT INTO tmp_session SET keywert = :sessionkey, ext_user = :ext, apikey = :apikey";
        $success = false;

        $apikey = $this->createApiKey();

        $sth = $this->PDO->prepare($SQL);
        $success = $sth->execute(array(":sessionkey" => $sessionkey, ":ext" => $id_ext, ":apikey" => $apikey));

        return $success;
    }

    public function renewSession($id_ext, $oldSessionKey) {
        $ret = 0;
        $newKey = $this->createSessionKey();
        $sess = $this->selectSession($id_ext);

        if($sess->keywert == $oldSessionKey) {
            $this->updateSessionOnly($id_ext, $newKey);
            $_SESSION['id'] = $id_ext;
            $_SESSION['key'] = $newKey;
            $ret = $newKey;
        }

        return $ret;
    }

    public function createSessionKey() {
        return rand(1000000000, 4294967200) + (100000*rand(1000000, 4294967200)) + (time() % 5000);
    }

    public function createApiKey() {
        return rand(1000000000, 4294967288800) + (100000*rand(1002323, 4294967200)) + (time() % 5000);
    }
}