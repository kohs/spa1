<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

class AbsDbClass
{
    protected static function queryAndFetchArray($SQL, $idLabel = "id") {
        $PDO = getPHPDBOBJ("");
        $sth = $PDO->query($SQL);

        $array = array();
        while($data = $sth->fetchObject()) {
            if(isset($data->$idLabel)) {
                $array[] = $data;
            }
        }
        return $array;
    }

    protected static function queryAndFetchMe($SQL) {
        $PDO = getPHPDBOBJ("");
        $sth = $PDO->query($SQL);

        $data = $sth->fetchObject();
        if(isset($data->id)) {
            return $data;
        }
        return null;
    }

    protected static function prepareAndFetchMe($SQL, $dataArray) {
        $PDO = getPHPDBOBJ("");

        $sth = $PDO->prepare($SQL);
        $sth->execute($dataArray);

        $data = $sth->fetchObject();
        if(!empty($data)) {
            return $data;
        }
        return null;
    }

    protected static function prepareAndFetchArray($SQL, $dataArray) {
        $PDO = getPHPDBOBJ("");

        $sth = $PDO->prepare($SQL);
        $sth->execute($dataArray);

        $array = array();
        while($data = $sth->fetchObject()) {
            if(!empty($data)) {
                $array[] = $data;
            }
        }
        return $array;
    }
}