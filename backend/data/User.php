<?php
/*
Copyright 2016 Mathias Kohs

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require_once __DIR__.'/../DB-DUMMY.php';

class User {

    private $PDO;
    protected $DbEdit;
    private $id;
    private $tblName = "tbl_user";

    public function __construct($Id) {
        $this->PDO = getPHPDBOBJ("");
        $this->DbEdit = getDbEditInsert();
        $this->id = $Id;
    }

    public function getData() {
        $SQL = "SELECT * FROM tbl_user WHERE id = :id";

        $sth = $this->PDO->prepare($SQL);
        $sth->execute(array(":id" => $this->id));

        $data = $sth->fetchObject();

        if(empty($data) || $data->id != $this->id) {
            return null;
        }
        return $data;
    }

    public static function addData($data) {
        $DbEdit = getDbEditInsert();
        $dataChecked = new stdClass();

        $errorCode = 1; //groesser als 0 ist OK

        if(isset($data->login)) {
            $dataChecked->id_ext = uniqid(md5($data->login), true);
            $dataChecked->login = $data->login;
        }else{
            $errorCode = -3;
        }
        isset($data->passwort) ? $dataChecked->passwort = (User::cryptPasswort($data->passwort)) : $errorCode = -3;
        isset($data->fs_person) ? $dataChecked->fs_person = $data->fs_person : $errorCode = -3;

        if($errorCode > 0){
            $insId = $DbEdit->addInsert("tbl_user", $dataChecked);
            if($insId > 0){
                $errorCode = $insId;
            }else{
                $errorCode = -1;
            }
        }

        return $errorCode;
    }

    private static function cryptPasswort($clear) {
        $salt = "asdasdasd";
        $rounds = "2000";
        $salt = '$rounds=' . $rounds . '$' . $salt . '$';
        return crypt($clear, '$6' . $salt);
    }

    public function editData($data) {
        $dataChecked = new stdClass();
        if(isset($this->id)) {
            isset($data->login) ? $dataChecked->login = $data->login : null ;
            if(isset($data->passwort)){

                $dataChecked->passwort = User::cryptPasswort($data->passwort);
            }

            $where = array("id" => $this->id);

            return $this->DbEdit->addUpdate($this->tblName, $dataChecked, $where);
        }
        return false;
    }
}
